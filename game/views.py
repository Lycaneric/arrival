from django.shortcuts import render, redirect

from arrival.functions import fetch_context

def index(request):
    template = 'game/index.html'
    context = fetch_context(request)
    return render(request, template, context)


def game_index(request):
    template = ''
    context = fetch_context(request)
    return render(request, template, context)
